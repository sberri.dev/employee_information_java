
class EmployeeIformation {

    public static void main(String[] args) {
        String[] employeeNames = { "Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly" };
        int[] hoursWorked = { 35, 38, 35, 38, 40 };
        double[] hourlyRates = { 12.5, 15.0, 13.5, 14.5, 13.0 };
        String[] positions = { "Caissier", "Projectionniste", "Caissier", "Manager", "Caissier" };
        String searchPosition = "Caissier";

        //Condition pour calculer le salaire hebdomadaire de chaque employé 
        for (int i = 0; i < employeeNames.length; i++) {

            double weeklySalary;
            if (hoursWorked[i] > 35) {
                weeklySalary = (hoursWorked[i] * hourlyRates[i]) * 1.5;

            } else {
                weeklySalary = hoursWorked[i] * hourlyRates[i];
            }
            System.out.println(employeeNames[i] + ": " + weeklySalary + " €");
        }

        for (int i = 0; i < searchPosition.length(); i++) {
            if (positions[i].equals(searchPosition)) {
                System.out.println(employeeNames[i]);
            } else {
                System.out.println("Aucun employé trouvé.");
            }
        }
    }
}